<?php
  if (isset($_POST['name']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['message']) && isset($_POST['imaName']) && isset($_POST['companyName']) && isset($_POST['phoneNum'])) {
    if (!empty(['name']) && !empty(['lastname']) && !empty(['email']) && !empty(['message']) && !empty($_POST['imaName']) && !empty($_POST['companyName']) && !empty($_POST['phoneNum'])) {

      ini_set( 'display_errors', 1 );
      error_reporting( E_ALL );

      // take value from input 
      $fname = $_POST['name'];
      $lname = $_POST['lastname'];
      $email = $_POST['email'];
      $imaName = $_POST['imaName'];
      $companyName = $_POST['companyName'];
      $phoneNum = $_POST['phoneNum'];
      $subj = "Hashmove";
      $msg = $_POST['message'];

      $to = 'daniyal@texpo.com';
      $from = "sales@hashmove.com";
      $headers = "From:" . $from;

      if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        echo "Provide Validate Email";
      }
      else{
        $body = "Name:"." ".$fname." ".$lname."\n"."Email Address:"." ".$email."\n"."Phone:"." ".$phoneNum."\n"."I am:"." ".$imaName."\n"."Company:"." ".$imaName."\n"."Message:"." ".$msg;
        if(mail($to,$subj,$body ,$headers)){
          echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><p>Thank you <strong>'.$fname." ".$lname.'</strong>, your message has been submitted to us.</p></div>';
        }
        else{
          echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><div class="alert alert-danger"><strong>ERROR!</strong> The email was not sent, either try again or later.</div>';
        }
      }
    }
    else{
      // Validate inputs
      if(empty($fname)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Name is required.</p></div>';
        exit();
      }
      if(empty($lname)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Last name is required.</p></div>';
        exit();
      }
      if(empty($email)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Email is required.</p></div>';
        exit();
      }
      if(empty($imaName)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Email is required.</p></div>';
        exit();
      }
      if(empty($phoneNum)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Email is required.</p></div>';
        exit();
      }
      if(empty($companyName)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Email is required.</p></div>';
        exit();
      }
      if(empty($subj)){
        $subj = 'You have been contacted from your website by ' . $fname . ' ' . $lname;
      }
      if(empty($msg)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Message is required.</p></div>';
        exit();
      }
    }
  }
  else{
   echo 'not submited'; 
  }
?>
