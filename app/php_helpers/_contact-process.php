<?php
  if (isset($_POST['name']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message'])) {
    if (!empty(['name']) && !empty(['lastname']) && !empty(['email']) && !empty(['subject']) && !empty(['message'])) {

      ini_set( 'display_errors', 1 );
      error_reporting( E_ALL );

      // take value from input 
      $fname = $_POST['name'];
      $lname = $_POST['lastname'];
      $email = $_POST['email'];
      $subj = $_POST['subject'];
      $msg = $_POST['message'];

      $to = 'info@hashmove.com';
      $from = "sales@hashmove.com";
      $headers = "From:" . $from;

      if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        echo "Provide Validate Email";
      }
      else{
        $body = "Name:"." ".$fname." ".$lname."\n"."Email Address:"." ".$email."\n"."Subject:"." ".$subj."\n"."Message:"." ".$msg;
        if(mail($to, $subj,  $body ,$headers)){
          echo '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><p>Thank you <strong>'.$fname." ".$lname.'</strong>, your message has been submitted to us.</p></div>';
        }
        else{
          echo '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><div class="alert alert-danger"><strong>ERROR!</strong> The email was not sent, either try again or later.</div>';
        }
      }
    }
    else{
      // Validate inputs
      if(empty($fname)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Name is required.</p></div>';
        exit();
      }
      if(empty($lname)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Last name is required.</p></div>';
        exit();
      }
      if(empty($email)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Email is required.</p></div>';
        exit();
      }
      if(empty($subj)){
        $subj = 'You have been contacted from your website by ' . $fname . ' ' . $lname;
      }
      if(empty($msg)){
        echo '<div class="alert alert-warning error"><p><strong>Attention!</strong> Message is required.</p></div>';
        exit();
      }
    }
  }
  else{
   echo 'not submited'; 
  }
?>
