;(function($){
	"use strict";
	
	// Shrink header on scroll - Start
	$(document).on("scroll", function () {
		if ($(document).scrollTop() > 100) {
			$("header").addClass("shrink");
		} else if ($(window).width() < 993) {
			$("header").addClass("shrink");
		} else {
			$("header").removeClass("shrink");
		}
	});
	// Shrink header on scroll - / End


	// Custom Toggle for benefit section - Read More / Read Less 
	$(".benefit-read-more").click(function () {
		$(this).text($(this).text() == 'Read More' ? 'Read Less' : 'Read More');
		$(this).siblings("p").toggleClass("benefit-read-less");
	});


	$(document).ready(function() {

		
			// click on button submit
			$("#susb_submit").on('click', function () {

				$.ajax({
					url: "https://hashmove.com/php_helpers/_subscription.php",
					type: 'POST',
					data: $("#subs_form").serialize(),
					success: function (res) {
						alert(res);
						console.log(res);
						e.preventDefault();
						e.stopPropagation();
					},
					error: function (err) {
						e.preventDefault();
						e.stopPropagation();
						console.log(error);
					}
				});
			});
		

		var acc2 = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc2.length; i++) {
			acc2[i].addEventListener("click", function () {
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight) {
					panel.style.maxHeight = null;
				} else {
					for (var j = 0; j < acc2.length; j++) {
						acc2[j].classList.remove("active")
						var nextelem = acc2[j].nextElementSibling;
						nextelem.style.maxHeight = null
					}
					panel.style.maxHeight = panel.scrollHeight + "px";
				}
				this.classList.toggle("active");
			});
		}

		$('.inner-slide').mouseover(function () {
			$(this).children().children().children().addClass('deactiveGreyScale');
		});
		$('.inner-slide').mouseleave(function () {
			$(this).children().children().children().removeClass('deactiveGreyScale');
		});


	});// end of document ready

})(jQuery);
