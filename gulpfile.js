var gulp = require('gulp'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  htmlmin = require('gulp-htmlmin'),
  autoprefixer = require('gulp-autoprefixer'),
  browserSync = require('browser-sync').create(),
  useref = require('gulp-useref'),
  uglify = require('gulp-uglify'),
  gulpIf = require('gulp-if'),
  cssnano = require('gulp-cssnano'),
  imagemin = require('gulp-imagemin'),
  del = require('del'),
  runSequence = require('run-sequence'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  stylus = require('gulp-stylus');


function errorlog(error) {
  console.error.bind(error);
  this.emit('end');
}


//SASS 
gulp.task('sass', function () {

  return gulp.src('app/scss/**/*.scss')
    .pipe(sass())
    .on('error', errorlog)
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(gulp.dest('app/css/'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//Stylus
gulp.task('stylus', function () {

  return gulp.src('app/stylus/*.styl')
    .pipe(stylus())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

//jshint
gulp.task('jshint', function () {
  return gulp.src('app/js/main.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});



//useref JS and CSS
gulp.task('useref', function () {
  return gulp.src('app/*.html')
    .pipe(useref())

    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});


//IMG min
gulp.task('images', function () {
  return gulp.src('app/images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(imagemin({
      // Setting interlaced to true
      interlaced: true
    }))
    .pipe(gulp.dest('dist/images'))
});

//Fonts
gulp.task('fonts', function () {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
})

// Gulp task to minify styles files
gulp.task('styles', function () {
  return gulp.src('app/css/**/*.scss')
    // Compile SASS files
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    // Auto-prefix css styles for cross browser compatibility
    .pipe(autoprefixer({
      browsers: AUTOPREFIXER_BROWSERS
    }))
    // Minify the file
    .pipe(csso())
    // Output
    .pipe(gulp.dest('./dist/css'))
});

gulp.task('cssMin', function () {
  return gulp.src('app/css/**/*.css')
    .pipe(sourcemaps.init())
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'));
});
// Gulp task to minify HTML files
gulp.task('pages', function () {
  return gulp.src(['app/**/*.html'])
    // return gulp.src(['app/**/*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist'));
});
// scripts
// Gulp task to minify JavaScript files
gulp.task('scripts', function () {
  return gulp.src('app/js/**/*.js')
    // Minify the file
    .pipe(uglify())
    // Output
    .pipe(gulp.dest('dist/js'))
});



//BrowserSync
gulp.task('watch', ['browserSync'], function () {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/stylus/**/*.styl', ['stylus']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('browserSync', function () {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
});

//Clean:dist
gulp.task('clean:dist', function () {
  return del.sync('dist');
});


//Build


gulp.task('build', function (callback) {
  runSequence('clean:dist', ['cssMin', 'sass', 'pages', 'scripts', 'fonts', 'images'],
    callback
  )
});